<?php

namespace App\Entity;

use App\Repository\RateTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RateTypeRepository::class)]
class RateType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 64)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $fullname = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $notes = null;

    #[ORM\OneToMany(mappedBy: 'rateType', targetEntity: RateZone::class)]
    private Collection $rateZones;

    #[ORM\OneToMany(mappedBy: 'rateType', targetEntity: RateRule::class)]
    private Collection $rateRules;

    public function __construct(String $name, String $fullname = '', String $notes = '')
    {
        $this->setName($name);
        $this->setFullname($fullname);
        $this->setNotes($notes);
        $this->rateZones = new ArrayCollection();
        $this->rateRules = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }



    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(?string $fullname): static
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * @return Collection<int, RateZone>
     */
    public function getRateZones(): Collection
    {
        return $this->rateZones;
    }

    public function addRateZone(RateZone $rateZone): static
    {
        if (!$this->rateZones->contains($rateZone)) {
            $this->rateZones->add($rateZone);
            $rateZone->setRateType($this);
        }

        return $this;
    }

    public function removeRateZone(RateZone $rateZone): static
    {
        if ($this->rateZones->removeElement($rateZone)) {
            // set the owning side to null (unless already changed)
            if ($rateZone->getRateType() === $this) {
                $rateZone->setRateType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RateRule>
     */
    public function getRateRules(): Collection
    {
        return $this->rateRules;
    }

    public function addRateRule(RateRule $rateRule): static
    {
        if (!$this->rateRules->contains($rateRule)) {
            $this->rateRules->add($rateRule);
            $rateRule->setRateType($this);
        }

        return $this;
    }

    public function removeRateRule(RateRule $rateRule): static
    {
        if ($this->rateRules->removeElement($rateRule)) {
            // set the owning side to null (unless already changed)
            if ($rateRule->getRateType() === $this) {
                $rateRule->setRateType(null);
            }
        }

        return $this;
    }
}
