<?php

namespace App\Entity;

use App\Repository\ShipmentTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ShipmentTypeRepository::class)]
class ShipmentType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 64)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'shipmentType', targetEntity: RateRule::class)]
    private Collection $rateRules;

    #[ORM\Column(type: Types::ARRAY)]
    private array $accepted_weights = [];

    public function __toString()
    {
        return $this->getName();
    }
    public function __construct($name)
    {
        $this->setName(($name));
        $this->rateRules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, RateRule>
     */
    public function getRateRules(): Collection
    {
        return $this->rateRules;
    }

    public function addRateRule(RateRule $rateRule): static
    {
        if (!$this->rateRules->contains($rateRule)) {
            $this->rateRules->add($rateRule);
            $rateRule->setShipmentType($this);
        }

        return $this;
    }

    public function removeRateRule(RateRule $rateRule): static
    {
        if ($this->rateRules->removeElement($rateRule)) {
            // set the owning side to null (unless already changed)
            if ($rateRule->getShipmentType() === $this) {
                $rateRule->setShipmentType(null);
            }
        }

        return $this;
    }

    public function getAcceptedWeights(): array
    {
        return $this->accepted_weights;
    }

    public function setAcceptedWeights(array $accepted_weights): static
    {
        $this->accepted_weights = $accepted_weights;

        return $this;
    }

    public function addWeight(float $weight){
        $this->accepted_weights[] = $weight;
    }
}
