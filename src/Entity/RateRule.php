<?php

namespace App\Entity;

use App\Repository\RateRuleRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RateRuleRepository::class)]
class RateRule
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?float $weight = null;

    #[ORM\Column(length: 64)]
    private ?string $rate = null;

    #[ORM\ManyToOne(inversedBy: 'rateRules')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ShipmentType $shipmentType = null;

    #[ORM\ManyToOne(inversedBy: 'rateRules')]
    #[ORM\JoinColumn(nullable: false)]
    private ?RateZone $zone = null;

    #[ORM\ManyToOne(inversedBy: 'rateRules')]
    private ?RateType $rateType = null;

    public function __construct(float $rate,ShipmentType $currentPackageType,float $currentWeight,RateZone $zone,RateType $currentCategory)
    {
        $this->setRate($rate);
        $this->setShipmentType($currentPackageType);
        $this->setWeight($currentWeight);
        $this->setZone($zone);
        $this->setRateType($currentCategory);
    }

    public function __toString()
    {
        return "Rate from Zone ".$this->zone." of type ".$this->shipmentType;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): static
    {
        $this->weight = $weight;

        return $this;
    }

    public function getRate(): ?string
    {
        return $this->rate;
    }

    public function setRate(string $rate): static
    {
        $this->rate = $rate;

        return $this;
    }

    public function getShipmentType(): ?ShipmentType
    {
        return $this->shipmentType;
    }

    public function setShipmentType(?ShipmentType $shipmentType): static
    {
        $this->shipmentType = $shipmentType;

        return $this;
    }

    public function getZone(): ?RateZone
    {
        return $this->zone;
    }

    public function setZone(?RateZone $zone): static
    {
        $this->zone = $zone;

        return $this;
    }

    public function getRateType(): ?RateType
    {
        return $this->rateType;
    }

    public function setRateType(?RateType $rateType): static
    {
        $this->rateType = $rateType;

        return $this;
    }
}
