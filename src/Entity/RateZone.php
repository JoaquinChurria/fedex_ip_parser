<?php

namespace App\Entity;

use App\Repository\RateZoneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RateZoneRepository::class)]
class RateZone
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 3)]
    private ?string $zone = null;

    #[ORM\ManyToOne(inversedBy: 'rateZones')]
    #[ORM\JoinColumn(nullable: false)]
    private ?RateType $rateType = null;

    #[ORM\OneToMany(mappedBy: 'zone', targetEntity: RateRule::class)]
    private Collection $rateRules;

    #[ORM\Column(length: 64, nullable: true)]
    private ?string $majorDestinations = null;

    public function __construct($zone, $rateType)
    {
        $this->setZone($zone);
        $this->setRateType($rateType);
        $this->rateRules = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getZone().' - '.$this->getRateType()->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getZone(): ?string
    {
        return $this->zone;
    }

    public function setZone(string $zone): static
    {
        $this->zone = $zone;

        return $this;
    }

    public function getRateType(): ?RateType
    {
        return $this->rateType;
    }

    public function setRateType(?RateType $rateType): static
    {
        $this->rateType = $rateType;

        return $this;
    }

    /**
     * @return Collection<int, RateRule>
     */
    public function getRateRules(): Collection
    {
        return $this->rateRules;
    }

    public function addRateRule(RateRule $rateRule): static
    {
        if (!$this->rateRules->contains($rateRule)) {
            $this->rateRules->add($rateRule);
            $rateRule->setZone($this);
        }

        return $this;
    }

    public function removeRateRule(RateRule $rateRule): static
    {
        if ($this->rateRules->removeElement($rateRule)) {
            // set the owning side to null (unless already changed)
            if ($rateRule->getZone() === $this) {
                $rateRule->setZone(null);
            }
        }

        return $this;
    }

    public function getMajorDestinations(): ?string
    {
        return $this->majorDestinations;
    }

    public function setMajorDestinations(?string $majorDestinations): static
    {
        $this->majorDestinations = $majorDestinations;

        return $this;
    }
}
