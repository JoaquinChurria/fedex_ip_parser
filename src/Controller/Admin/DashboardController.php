<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\RateRule;
use App\Entity\RateType;
use App\Entity\RateZone;
use App\Entity\ShipmentType;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;


class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        //return parent::index();
        
        $routeBuilder = $this->container->get(AdminUrlGenerator::class);
        $url = $routeBuilder->setController(RateRuleCrudController::class)->generateUrl();

        return $this->redirect($url);
        

        //return $this->render('../../templates/upload_csv.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Fedex Rates Parser');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
        //yield MenuItem::linktoRoute('Back to the website', 'fas fa-home', 'homepage');
        yield MenuItem::linkToCrud('Rate Rules', 'fas fa-envelope', RateRule::class);
        yield MenuItem::linkToCrud('Rate Types', 'fas fa-comments', RateType::class);
        yield MenuItem::linkToCrud('Zones', 'fas fa-map-marker-alt', RateZone::class);
        yield MenuItem::linkToCrud('Shipment Types', 'fas fa-box', ShipmentType::class);
        yield MenuItem::linkToRoute('Upload CSV', 'fa fa-file-upload', 'upload_fedex_csv');
    }
}
