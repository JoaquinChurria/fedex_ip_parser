<?php

namespace App\Controller\Admin;

use App\Entity\RateZone;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class RateZoneCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RateZone::class;
    }

    public function configureFields(string $pageName): iterable{
        return [
            TextField::new('Zone'),
            TextField::new('Major_Destinations'),
            AssociationField::new('rateType','Rate Type'),
        ];
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
