<?php

namespace App\Controller\Admin;

use App\Entity\ShipmentType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class ShipmentTypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string{
        return ShipmentType::class;
    }

    
    public function configureFields(string $pageName): iterable{
        return [
            TextField::new('name'),
        ];
    }
    
    public function configureCrud(Crud $crud): Crud{
        return $crud
            // the labels used to refer to this entity in titles, buttons, etc.
            ->setEntityLabelInSingular('Shipment Type')
            ->setEntityLabelInPlural('Shipment Types')

            // the Symfony Security permission needed to manage the entity
            // (none by default, so you can manage all instances of the entity)
            //->setEntityPermission('ROLE_EDITOR')
        ;
    }
}
