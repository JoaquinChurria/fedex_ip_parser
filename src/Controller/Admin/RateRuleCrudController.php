<?php

namespace App\Controller\Admin;

use App\Entity\RateRule;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class RateRuleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RateRule::class;
    }

    public function configureFields(string $pageName): iterable{
        return [
            NumberField::new('Weight'),
            TextField::new('Rate'),
            AssociationField::new('shipmentType','Shipment Type'),
            AssociationField::new('zone','Zone'),
        ];
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
