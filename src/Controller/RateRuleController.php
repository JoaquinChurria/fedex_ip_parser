<?php
// src/Controller/RateRuleController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Scripts\Fedex_CSV_Parser;
use Doctrine\ORM\EntityManagerInterface;

class RateRuleController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

    }


    #[Route("/admin/upload-fedex-csv", name:"upload_fedex_csv")]
    public function uploadCsv(Request $request): Response
    {
        if ($request->isMethod('POST')) {
            /** @var UploadedFile $csvFile */
            $csvFile = $request->files->get('csv_file');

            // Your logic to handle and save the CSV file
            // For example, you can move the file to a specific directory
            $csvFile->move($_ENV['CSV_UPLOAD_DIRECTORY'], 'fedex_rates_'.date("Ymd").'.csv');

            // Additional logic if needed
            $parser = new Fedex_CSV_Parser($this->entityManager);
            $parser->execute_fedex_csv_parser($_ENV['CSV_UPLOAD_DIRECTORY'].'fedex_rates_'.date("Ymd").'.csv');

            // Redirect or render a response
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/upload_csv.html.twig');
    }
    // Add additional methods as needed
}