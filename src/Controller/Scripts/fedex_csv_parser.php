<?php

namespace App\Scripts;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\RateRule;
use App\Entity\RateType;
use App\Entity\RateZone;
use App\Entity\ShipmentType;
use Exception;

class Fedex_CSV_Parser{

    private $entityManager;
    private $rateRulesArray = [];

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

    }

    /**
     * Get the value of rateRulesArray
     */ 
    public function getRateRulesArray()
    {
        return $this->rateRulesArray;
    }

    /**
     * Set the value of rateRulesArray
     *
     * @return  self
     */ 
    public function setRateRulesArray($rateRulesArray)
    {
        $this->rateRulesArray = $rateRulesArray;

        return $this;
    }

  
    public function addRateRule($rateRule)
    {
        $this->rateRulesArray[] = $rateRule;

        return $this;
    }


    public function execute_fedex_csv_parser($filepath = ''){

        $this->parse_fedex_csv($filepath);
        $this->sendToDatabase();

        return true;

    }
    


    public function sendToDatabase(){
        $rateRules = $this->getRateRulesArray();
        $em = $this->entityManager;
        foreach ($rateRules as $rateRule) {
            
            //persist related entities while checking for duplicates
            //In order: RateType, then RateZone, then ShipmentType, then RateRule. This is to be able to create unique IDs to reuse *before* getting to the rateRule
            $repoRateType = $em->getRepository(RateType::class);
            $rateType = $repoRateType->findOneBy(['name'=>$rateRule->getRateType()->getName()]);
            if ($rateType != null){
                $rateRule->setRateType($rateType);
            }else{
                $rateType = $rateRule->getRateType();
                $em->persist($rateType);
            }

            $repoRateZone = $em->getRepository(RateZone::class);
            $rateZone = $repoRateZone->findOneBy(['zone'=>$rateRule->getZone()->getZone(),'rateType'=>$rateType]);
            if ($rateZone != null){
                $rateRule->setZone($rateZone);
            }else{
                $rateZone = $rateRule->getZone();
                $rateZone->setRateType($rateType);
                $em->persist($rateZone);
            }

            $repoShipmentType = $em->getRepository(ShipmentType::class);
            $shipmentType = $repoShipmentType->findOneBy(['name'=>$rateRule->getShipmentType()->getName()]);
            if ($shipmentType != null){
                $rateRule->setShipmentType($shipmentType);
            }else{
                $shipmentType = $rateRule->getShipmentType();
                $em->persist($shipmentType);
            }
            
            
        }

        // Flush changes to the database
        $em->flush();

        foreach ($rateRules as $rateRule) {
            $em->persist($rateRule);
        }
        // Flush changes to the database
        $em->flush();

        return 0;
    }


    public function parse_fedex_csv($filepath = ''){

        if ($filepath != ''){
            $csvFile = $filepath;
        }else{
            throw new Exception("Error: Invalid filepath",1);
        }
        
        
        $handle = fopen($csvFile, 'r');
        
        if ($handle !== false) {
            $processedCategories = [];
            $processedZones = [];
            $processedPackageTypes = [];
            $currentCategory = new RateType('','','');
            $currentPackageType = new ShipmentType('');
            $currentWeight = null;
            $notes = '';
            $zoneHeaders = [];
        
            while (($data = fgetcsv($handle, 0, ',')) !== false) {
                if (!array_key_exists(3,$data))throw new Exception("Error: File format is incorrect, expecting a CSV with multiple columns",2);
                if (empty($data[0]) && empty($data[1]) && empty($data[3])) {
                    $data = fgetcsv($handle, 0, ',');
                    continue;
                }
        
                $dataType = strtolower(trim($data[0]));
        
                if (empty($dataType)) {
                    $dataType = strtolower(trim($data[1]));
                }
        
                switch ($dataType) {
                    case 'fedex confidential information':
        
                            $data = fgetcsv($handle, 0, ',');
                            if (!empty($data[1])) {
                                // This is the start of a new shipping category
                                $currentCategory = new RateType(
                                    strtolower(trim($data[1])),
                                    strtolower(trim($data[1])),
                                    '' // Initialize notes for the new category
                                );
                                
                                // Track processed categories
                                $processedCategories[] = $currentCategory;
                            };
                        break;
        
                    case 'japan':
                        // Process country if needed
        
                        $fullname = '';
                        $data = fgetcsv($handle, 0, ',');
                        $data = fgetcsv($handle, 0, ',');
                            if (!empty($data[1])) {
                                // This is the full length name
                                $fullname = $data[1];
                                $data = fgetcsv($handle, 0, ',');
                                if (!empty($data[1])) {
                                    // Second row
                                    $fullname = $fullname.', '.$data[1];
            
                                };
                            };
        
                            $currentCategory->setFullName($fullname);
        
                        $data = fgetcsv($handle, 0, ',');
                        if (!empty($data[1])) {
                            // This row has a small note.
                            $currentCategory->setNotes($data[1]);
                        };
        
                        //skip table header
                        $data = fgetcsv($handle, 0, ',');
                        break;
        
                    case 'rates in jye':
                        $zoneHeaders = array_slice($data, 3);
                        break;
        
                    default:
                    if (empty($data[3])) break;
                        $packageTypeName = strtolower(trim($data[1]));
        
                        if (empty($packageTypeName)) {
                            // Use the last ShipmentType if empty
                            $packageTypeName = $currentPackageType->getName();
                        } else {
                            if (str_contains($packageTypeName, "rates per kg ")){
                                $data = fgetcsv($handle, 0, ',');
                                $packageTypeName = "weight (kg)";
                            }
                            // Create ShipmentType object
                            $currentPackageType = new ShipmentType($packageTypeName);
        
                            // Track processed package types
                            $processedPackageTypes[] = $currentPackageType;
                        }
        
                        // Add weight to ShipmentType
                        $currentWeight = floatval($data[2]);
                        if ($currentWeight == 0){
                            $arr = [];
                            preg_match_all('!\d+\.*\d*!', (String) $data[2], $arr);   
                        };
                        
                        $currentPackageType->addWeight($currentWeight);
                        


                        for ($i = 3; $i < count($data); $i++) {
                            $zone = $zoneHeaders[$i - 3];
                            if (!is_numeric($data[$i])) continue;
                            $rate = floatval($data[$i]);
        
                            // Create RateZone object
                            if (!array_key_exists($zone.$currentCategory->getName(),$processedZones)){
                                $currentZone = new RateZone($zone,$currentCategory);
                                // Track processed zones as a dictionary
                                $processedZones[$zone.$currentCategory->getName()] = $currentZone;
                            }
        
                            // Create RateRule object
                            $newRule = new RateRule(
                                $rate,
                                $currentPackageType,
                                $currentWeight,
                                $processedZones[$zone.$currentCategory->getName()],
                                $currentCategory
                            );
                            $this->addRateRule($newRule);
                        }
                        break;
                }
            }
        
            fclose($handle);
            // Now $rateRules contains an array of RateRule objects
            
            
            // Processed categories, zones, and package types
            //echo "Processed Categories: " . implode(', ', array_unique($processedCategories)) . PHP_EOL;
            //echo "Processed Zones: " . implode(', ', array_keys($processedZones)) . PHP_EOL;
            //echo "Processed Package Types: " . implode(', ', array_unique($processedPackageTypes)) . PHP_EOL;
            return 0;
        } else {
            echo "Error opening file.";
            return -1;
        
            
        }

    }

    
}




?>
