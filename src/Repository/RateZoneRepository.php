<?php

namespace App\Repository;

use App\Entity\RateZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RateZone>
 *
 * @method RateZone|null find($id, $lockMode = null, $lockVersion = null)
 * @method RateZone|null findOneBy(array $criteria, array $orderBy = null)
 * @method RateZone[]    findAll()
 * @method RateZone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateZoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RateZone::class);
    }

//    /**
//     * @return RateZone[] Returns an array of RateZone objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RateZone
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
