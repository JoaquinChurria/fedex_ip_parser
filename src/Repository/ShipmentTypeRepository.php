<?php

namespace App\Repository;

use App\Entity\ShipmentType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ShipmentType>
 *
 * @method ShipmentType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShipmentType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShipmentType[]    findAll()
 * @method ShipmentType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShipmentTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShipmentType::class);
    }

//    /**
//     * @return ShipmentType[] Returns an array of ShipmentType objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ShipmentType
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
