<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240121192438 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE rate_rule (id INT AUTO_INCREMENT NOT NULL, shipment_type_id INT NOT NULL, zone_id INT NOT NULL, weight DOUBLE PRECISION NOT NULL, rate VARCHAR(64) NOT NULL, INDEX IDX_B9DC1EC32EE48A36 (shipment_type_id), INDEX IDX_B9DC1EC39F2C3FAB (zone_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rate_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) NOT NULL, fullname VARCHAR(255) DEFAULT NULL, notes VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rate_zone (id INT AUTO_INCREMENT NOT NULL, rate_type_id INT NOT NULL, zone VARCHAR(3) NOT NULL, major_destinations VARCHAR(64) DEFAULT NULL, INDEX IDX_5FEF72081F8EC372 (rate_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shipment_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rate_rule ADD CONSTRAINT FK_B9DC1EC32EE48A36 FOREIGN KEY (shipment_type_id) REFERENCES shipment_type (id)');
        $this->addSql('ALTER TABLE rate_rule ADD CONSTRAINT FK_B9DC1EC39F2C3FAB FOREIGN KEY (zone_id) REFERENCES rate_zone (id)');
        $this->addSql('ALTER TABLE rate_zone ADD CONSTRAINT FK_5FEF72081F8EC372 FOREIGN KEY (rate_type_id) REFERENCES rate_type (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE rate_rule DROP FOREIGN KEY FK_B9DC1EC32EE48A36');
        $this->addSql('ALTER TABLE rate_rule DROP FOREIGN KEY FK_B9DC1EC39F2C3FAB');
        $this->addSql('ALTER TABLE rate_zone DROP FOREIGN KEY FK_5FEF72081F8EC372');
        $this->addSql('DROP TABLE rate_rule');
        $this->addSql('DROP TABLE rate_type');
        $this->addSql('DROP TABLE rate_zone');
        $this->addSql('DROP TABLE shipment_type');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
