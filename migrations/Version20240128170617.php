<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240128170617 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE csv_data (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rate_rule ADD rate_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE rate_rule ADD CONSTRAINT FK_B9DC1EC31F8EC372 FOREIGN KEY (rate_type_id) REFERENCES rate_type (id)');
        $this->addSql('CREATE INDEX IDX_B9DC1EC31F8EC372 ON rate_rule (rate_type_id)');
        $this->addSql('ALTER TABLE shipment_type ADD accepted_weights LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE csv_data');
        $this->addSql('ALTER TABLE shipment_type DROP accepted_weights');
        $this->addSql('ALTER TABLE rate_rule DROP FOREIGN KEY FK_B9DC1EC31F8EC372');
        $this->addSql('DROP INDEX IDX_B9DC1EC31F8EC372 ON rate_rule');
        $this->addSql('ALTER TABLE rate_rule DROP rate_type_id');
    }
}
