# Project Setup

## Prerequisites
- PHP 8.3.1+
- MySQL

## Steps to Set Up the Project

1. **Clone this repository:**
```bash
git clone https://gitlab.com/JoaquinChurria/fedex_ip_parser.git
cd fedex_ip_parser
```

2. **Start your MySQL server:**
Make sure your MySQL server is up and running.

3. **Create MySQL Database and User:**
Open the .env file and update the following parameters if needed:
```
DATABASE_URL=mysql://admin:rhd@127.0.0.1:3306/fedex_rates_parsing
```

Then, run the following commands:
```bash
mysql -u root -p
```
Enter your MySQL root password when prompted, then:
```sql
CREATE DATABASE fedex_rates_parsing;
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'rhd';
GRANT ALL PRIVILEGES ON fedex_rates_parsing.* TO 'admin'@'localhost';
FLUSH PRIVILEGES;
```

4. **Prepare the Database:**
Run the following command in the terminal to apply migrations:
```bash
php bin/console doctrine:migrations:migrate
```

5. **Run the App:**
The app is now ready to be run. Start a test server using the following command:
```bash
symfony server:start
```

# Using the App

1. **Start the App Server:**
If not already started, run the following command for a quick test server:
```bash
symfony server:start
```

2. **Access the App:**
Open your web browser and navigate to http://localhost:8000/admin.

3. **Navigate to Upload CSV Page:**
On the left panel, you will see CRUDs for each entity. The primary function of this app is the "Upload CSV" feature. Click on the corresponding link to access the upload page.

4. **Upload CSV File:**
    You will be prompted to select and upload a file.
    Upload a FedEx-like formatted CSV spreadsheet.
    If the file is of the wrong format or not a CSV, an exception will be thrown, and any changes to the database will be canceled.

5. **View Uploaded Data:**
    Upon successful upload, you will be redirected to the rate rules page.
    The page will display a table filled with the uploaded data.